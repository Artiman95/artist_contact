from odoo import models, fields, api


class SongArtist(models.Model):
    _name = 'song.artist'
    _description = 'Song artists'

    name = fields.Char(string='Name')
    duration = fields.Float(string='Duration')
    listeners = fields.Integer(string='Listeners')
    artist_id = fields.Many2one(comodel_name='res.partner', string='Artist')
    album_id = fields.Many2one(comodel_name='album.artist', string='Album', inverse_name='album_name')
    # member_ids = fields.One2many(comodel_name='res.partner', string='Members', inverse_name='member_id')
