from odoo import models, fields, api


class AlbumArtist(models.Model):
    _name = 'album.artist'
    _description = 'Album artists'

    name = fields.Char(string='Album Name')
    release_date = fields.Date(string='Release date')
    song_ids = fields.One2many(comodel_name='song.artist', string='Song', inverse_name='album_id')
    artist_id = fields.Many2one(comodel_name='res.partner', string='Artist')
    image = fields.Image(string='Album Image', max_width=1920, max_height=1920)
    artist_ids = fields.Many2many(string="Artists", comodel_name="res.partner", compute="_compute_artist_ids")

    @api.depends("artist_id", "song_ids")
    def _compute_artist_ids(self):
        for record_id in self:
            record_id.artist_ids = record_id.song_ids.artist_id.ids


    # related = 'song_ids.album_id'
    # member_ids = fields.One2many(comodel_name='res.partner', string='Members', inverse_name='member_id')