from odoo import models, fields, api

SEX = [
    ('male', 'Male'),
    ('female', 'Female')
]


class ResPartner(models.Model):
    _inherit = 'res.partner'
    company_type = fields.Selection(string='Group',
        selection=[('person', 'Artist'), ('company', 'Group')],
        compute='_compute_company_type', inverse='_write_company_type')

    month_listeners = fields.Integer(string="Month listeners")
    age = fields.Integer(string="Age")
    sex = fields.Selection(SEX, string='SEX')
    singles_ids = fields.One2many(comodel_name='song.artist', inverse_name='artist_id', string='Singles')
    # member_id = fields.Many2one(comodel_name='song.artist', string='Member')
    album_ids = fields.One2many(comodel_name='album.artist', inverse_name='artist_id', string='Album')
    # artist_id = fields.Many2one(comodel_name='song.artist', string='Artist')
