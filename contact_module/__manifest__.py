# -*- coding: utf-8 -*-
{
    'name': 'Contact module task',
    'version': '14.0.1.0',
    'summary': 'Contact Module Software',
    'sequence': -90,
    'description': """Contact Module Software""",
    'category': 'Productivity',
    'website': 'https://www.google.com',
    'depends': ["base", "mail", "contacts"],
    'data': [
        'security/ir.model.access.csv',
        'wizard/wizard_contact_view.xml',
        'views/album_artist_view.xml',
        'views/res_partner_card_view.xml',
        'views/song_artist_view.xml',
        'views/res_partner_view.xml',
        'views/menu_view.xml',
        'report/report.xml',
        'report/song_details_template.xml',
        'report/album_details_template.xml'
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
