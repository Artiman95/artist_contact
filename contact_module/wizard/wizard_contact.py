from odoo import models, fields, api
import xml.etree.ElementTree as ET
import base64
from datetime import datetime


class ContactWizard(models.TransientModel):
    _name = 'wizard.contact'

    name = fields.Binary(string='Add file with contacts', required=True)

    def create_contacts(self):
        data = {'Artists': [], 'Groups': []}
        tree = ET.fromstring(base64.b64decode(self.name).decode('utf-8').strip('\n'))
        for artist in tree.findall('./Artists/artist'):
            artist_data = {
                'name': artist.find('name').text,
                'month_listeners': artist.find('month_listeners').text if artist.find(
                    'month_listeners') is not None else '',
                'age': artist.find('age').text,
                'sex': artist.find('sex').text,
                'country': artist.find('country').text,
                'singles': [],
                'albums': []
            }

            for song in artist.findall('.//singles/songs/song'):
                song_data = {
                    'name': song.find('name').text,
                    'duration': float(song.find('duration').text.replace(':', '.')),
                    'listeners': song.find('listeners').text
                }
                artist_data['singles'].append(song_data)

            for album in artist.findall('.//albums/album'):
                album_data = {
                    'name': album.find('name').text,
                    'release_date': album.find('release_date'),
                    'songs': []
                }

                for song in album.findall('songs/song'):

                    song_data = {
                        'name': song.find('name').text,
                        'duration': float(song.find('duration').text.replace(':', '.')),
                        'listeners': song.find('listeners').text
                    }
                    album_data['songs'].append(song_data)

                artist_data['albums'].append(album_data)

            data['Artists'].append(artist_data)

        # for group in tree.findall('.//group'):
        #     group_data = {
        #         'name': group.find('name').text,
        #         'month_listeners': group.find('month_listeners').text,
        #         'artists': []
        #     }
        #
        #     for artist in group.findall('.//artist'):
        #         artist_data = {
        #             'name': artist.find('name').text,
        #             'age': artist.find('age').text,
        #             'sex': artist.find('sex').text,
        #             'country': artist.find('country').text
        #         }
        #         group_data['artists'].append(artist_data)
        #
        #     group_data['albums'] = []
        #     for album in group.findall('.//albums/album'):
        #         album_data = {
        #             'name': album.find('name').text,
        #             'release_date': album.find('release_date').text,
        #             'songs': []
        #         }
        #
        #         for song in album.findall('songs/song'):
        #             song_data = {
        #                 'name': song.find('name').text,
        #                 'duration': song.find('duration').text,
        #                 'listeners': song.find('listeners').text
        #             }
        #             album_data['songs'].append(song_data)
        #
        #         group_data['albums'].append(album_data)
        #
        #     data['Groups'].append(group_data)

        album_id = self.env['album.artist']
        for record in data.get('Artists'):

            # singles_ids = self.env['song.artist'].create(record.get('singles'))
            create_vals = {
                'name': record.get('name'),
                'month_listeners': record.get('month_listeners'),
                'age': record.get('age'),
                'sex': record.get('sex'),
                'company_type': 'person',
                # 'singles_ids': [(6, 0, singles_ids.ids)]
            }
            res_partner_id = self.env['res.partner'].create(create_vals)
            singles_ids = self.env['song.artist'].create(record.get('singles'))
            for album in record.get('albums'):
                singles_ids += self.env['song.artist'].create(album.get("songs"))
                album_vals = {
                    # 'album_name': album.get('name'),
                    'name': album.get('name'),
                    'release_date': datetime.strptime(album.get('release_date').text.replace(' ', '').strip('\n'), '%m-%d-%Y') if album.get('release_date') is not None else False,
                    # 'song_ids': [(0, 0, vals) for vals in album.get("songs")],
                    'artist_id': res_partner_id.id
                }

                album_ids = self.env['album.artist'].create(album_vals)
            res_partner_id.write({'singles_ids': singles_ids.ids})


